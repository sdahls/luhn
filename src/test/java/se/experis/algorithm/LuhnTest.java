package se.experis.algorithm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnTest {

    @Test
    public void removeSpace() {
        assertEquals("1234", Luhn.removeSpace("12 34"));

    }

    @Test
    public void isNumberTrue() {
        assertEquals(true, Luhn.isNumber("1234"));

    }

    @Test
    public void isNumberFalse() {
        assertEquals(false, Luhn.isNumber("hej"));

    }


    @Test
    void checkLengthFalse() {
        assertEquals(false, Luhn.checkLength("12345"));
    }

    @Test
    void checkLengthTrue() {
        assertEquals(true, Luhn.checkLength("1234567891234567"));
    }

    @Test
    void chechLengthZero(){
        assertEquals(false, Luhn.checkLength(""));
    }


    @Test
    void getCheckdigit() {
        assertEquals(0, Luhn.getCheckdigit("1234"));
    }

    @Test
    void getCheckDigigt(){
        assertEquals(6,Luhn.getCheckdigit("1234567"));
    }

    @Test
    void getCheckDigitOne(){
        assertEquals(0,Luhn.getCheckdigit("1"));
    }
    @Test
    void getCheckDigitZero()throws IndexOutOfBoundsException{
        assertThrows(IndexOutOfBoundsException.class,
                ()->Luhn.getCheckdigit("")
        );
    }

}