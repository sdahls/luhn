package se.experis.algorithm;
import java.util.ArrayList;


public class Luhn {

    public static String removeSpace(String str){
        str = str.replaceAll("\\s", "");//remove spaces
        return str;
    }




    public static boolean isNumber(String string) {
        return string.matches("^\\d+$");//makning sure the string is full of numbers
    }


    public static boolean checkLength(String cCnumber){//seeing if the string has the same length as a creditcardnumber

        if(cCnumber.length()>=16){
            return true;}else {
            return false;
        }

    }


    public static int getCheckdigit(String cCNumber){
        int container;//variable for holding ints
        int provided;
        int evenCounter=0;//counts if the integer will be timed to
        int sum=0;
        int checkdigit;
        ArrayList<Integer> cCArrayList = new ArrayList<>();
        ArrayList<Integer> cCArraylistWork = new ArrayList<>();

        for (int i=0; i<cCNumber.length(); i++){//putting the string into an arraylist
            container=Character.getNumericValue(cCNumber.charAt(i));
            cCArrayList.add(container);
        }

        provided=cCArrayList.get(cCArrayList.size()-1);

        cCArrayList.remove(cCArrayList.size()-1);//removing the last digit



        for(int i=cCArrayList.size()-1; i>=0 ; i--){//counting backwars throught the arraylist
            container=cCArrayList.get(i); //assigning the value into a container int in order to make the code more readable
            evenCounter++;

            if(!(evenCounter%2==0)){//keeping track of which number in the arraylist to work with

                container=container*2;
                if(container>=10){//if the int is 10 or more
                    container=(container/10)+(container%10);//adding them together
                }

            }
            cCArraylistWork.add(container);//adding the numbers inot a new arraylist because it was easier
        }


        for (int i=0; i<cCArraylistWork.size();i++){//adding upp the sum
            sum=sum+cCArraylistWork.get(i);
        }

        checkdigit=(sum*9)%10;//getting the checkdigit


        return checkdigit;

    }






}